# kafka-sample
This is a simple application for kafka communication process.

**Task description**

The goal is to implement a Spring Boot multi-module application which receives a request via REST API, publishes it to Kafka and after consumption writes it to MySQL .
JSON example:

{
“name“ : “Alice“,
“surname“ : “Thompson“,
“wage“ : 5500.75,
“eventTime“ : “2012-04-23T18:25:43.511Z“
}

Wage in JSON does not contain taxes. DB has to store the wage in a tax considered form. The tax percent has to be fetched from a property file. Example: JSON value - 1000 and tax is 10 percent, thus the DB has to store 1100.

The application and whole infrastructure has to be dockerized. Tester has to be able to propagate it’s own property file. Code has to be clean and tested.

**Solution description**

There are 2 services:
1) **_Rest-service_**
   
    This service send wage info to kafka.
    
    You can use this swagger api for test purposes.

    http://localhost:1010/kafka-sample/swagger-ui.html

2) **_Database-service_**
    
    This service get request wage info from kafka, then update wage according tax.json and save this updated wage info to database.
    
    Also you can use swagger api for checking all wages from database.
    
    http://localhost:2010/kafka-sample/swagger-ui.html

Each service has own docker image. The application has one docker-compose file which started 5 containers:

1) Zookeeper
2) Kafka
3) MySql
4) Rest-service
5) Database-service

User can update tax.json file or create own file. But in case of creating new file user must fix volumes section in database-service in docker-compose

**How to use**
1) Go to root directory
2) Build project
   
    _mvn clean install_
3) Build and start docker-compose
   
   _docker-compose up_
4) Go to rest-service swagger and send a request
   
   http://localhost:1010/kafka-sample/swagger-ui.html
5) Go to database-service and check saved data
   http://localhost:2010/kafka-sample/swagger-ui.html

**Suggestion for enhancements**
1) Add more kafka brokers for stability
2) Add more producers and consumers(then we need more than 1 partition)
3) Enhance the database. We can separate the current table on 2 tables(employees and wages)
4) Add better error handling into services(Spring AOP)
5) Add monitoring and logging(ELK stack)
6) Add integration tests
7) Add security
