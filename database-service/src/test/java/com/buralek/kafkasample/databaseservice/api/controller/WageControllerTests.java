package com.buralek.kafkasample.databaseservice.api.controller;

import com.buralek.kafkasample.databaseservice.api.service.WageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WageController.class)
public class WageControllerTests {
    @MockBean
    private WageService wageService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void sendNewWageTest() throws Exception {
        this.mockMvc.perform(get("/wage-database/all"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
