package com.buralek.kafkasample.databaseservice.api.mapper;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;
import com.buralek.kafkasample.databaseservice.mapper.WageDtoMapper;
import com.buralek.kafkasample.databaseservice.mapper.WageDtoMapperImpl;
import com.buralek.kafkasample.databaseservice.service.database.model.WageEntity;
import com.buralek.kafkasample.databaseservice.service.kafka.dto.WageKafkaDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest (classes={
        WageDtoMapperImpl.class
})
public class WageDtoMapperTests {
    private final Integer ID = 1;
    private final String NAME = "TestName";
    private final String SURNAME = "TestSurname";
    private final Double WAGE = 1000.0;
    private final Date EVENT_TIME = new Date();

    @Autowired
    private WageDtoMapper wageDtoMapper;

    @Test
    void wageEntityMapToWageDtoTest() {
        WageEntity wageEntity = getWageEntity();
        WageDto wageDto = wageDtoMapper.map(wageEntity);
        assertNotNull(wageDto);
        assertEquals(wageDto.getId(), ID);
        assertEquals(wageDto.getName(), NAME);
        assertEquals(wageDto.getSurname(), SURNAME);
        assertEquals(wageDto.getWage(), WAGE);
        assertEquals(wageDto.getEventTime(), EVENT_TIME);
    }

    @Test
    void wageKafkaDtoMapToWageEntity() {
        WageKafkaDto wageKafkaDto = getWageKafkaDto();
        WageEntity wageEntity = wageDtoMapper.map(wageKafkaDto);
        assertNotNull(wageEntity);
        assertNull(wageEntity.getId());
        assertEquals(wageEntity.getName(), NAME);
        assertEquals(wageEntity.getSurname(), SURNAME);
        assertEquals(wageEntity.getWage(), WAGE);
        assertEquals(wageEntity.getEventTime(), EVENT_TIME);
    }

    private WageEntity getWageEntity() {
        WageEntity wageEntity = new WageEntity();
        wageEntity.setId(ID);
        wageEntity.setName(NAME);
        wageEntity.setSurname(SURNAME);
        wageEntity.setWage(WAGE);
        wageEntity.setEventTime(EVENT_TIME);
        return wageEntity;
    }

    private WageKafkaDto getWageKafkaDto() {
        WageKafkaDto wageKafkaDto = new WageKafkaDto();
        wageKafkaDto.setName(NAME);
        wageKafkaDto.setSurname(SURNAME);
        wageKafkaDto.setWage(WAGE);
        wageKafkaDto.setEventTime(EVENT_TIME);
        return wageKafkaDto;
    }
}
