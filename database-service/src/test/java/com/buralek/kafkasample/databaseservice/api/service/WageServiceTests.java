package com.buralek.kafkasample.databaseservice.api.service;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;
import com.buralek.kafkasample.databaseservice.service.database.model.WageEntity;
import com.buralek.kafkasample.databaseservice.service.database.repository.WageRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ActiveProfiles("test")
public class WageServiceTests {
    private final Integer ID = 1;
    private final String NAME = "TestName";
    private final String SURNAME = "TestSurname";
    private final Double WAGE = 1000.0;
    private final Date EVENT_TIME = new Date();

    @Autowired
    private WageService wageService;

    @MockBean
    private WageRepository wageRepository;

    @Test
    void getAllWagesTest() {
        List<WageEntity> wageEntities = getWageEntities();
        when(wageRepository.findAll()).thenReturn(wageEntities);

        List<WageDto> wageDtos = wageService.getAllWages();
        assertEquals(wageDtos.size(), 1);

        WageDto wageDto = wageDtos.get(0);
        assertEquals(wageDto.getId(), ID);
        assertEquals(wageDto.getName(), NAME);
        assertEquals(wageDto.getSurname(), SURNAME);
        assertEquals(wageDto.getWage(), WAGE);
        assertEquals(wageDto.getEventTime(), EVENT_TIME);
    }

    private List<WageEntity> getWageEntities() {
        WageEntity wageEntity = new WageEntity();
        wageEntity.setId(ID);
        wageEntity.setName(NAME);
        wageEntity.setSurname(SURNAME);
        wageEntity.setWage(WAGE);
        wageEntity.setEventTime(EVENT_TIME);

        List<WageEntity> wageEntities = new ArrayList<>();
        wageEntities.add(wageEntity);
        return wageEntities;
    }
}
