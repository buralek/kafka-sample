package com.buralek.kafkasample.databaseservice.service.tax;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {TaxServiceImpl.class})
@ActiveProfiles("test")
public class TaxServiceTests {
    @Autowired
    private TaxService taxService;

    @Test
    void getWageWithTaxTest() {
        final Double initWage = 1000.0;
        final Double wageForCheck = 1100.0;
        final Double resultWage = taxService.getWageWithTax(initWage);
        assertEquals(resultWage, wageForCheck);
    }
}
