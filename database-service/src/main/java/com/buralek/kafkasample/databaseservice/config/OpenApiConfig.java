package com.buralek.kafkasample.databaseservice.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Consumer Database service for Kafka sample project", version = "${service-settings.version}"))
public class OpenApiConfig {
}
