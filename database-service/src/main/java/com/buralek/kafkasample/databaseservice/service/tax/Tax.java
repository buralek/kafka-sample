package com.buralek.kafkasample.databaseservice.service.tax;

import lombok.Data;

@Data
public class Tax {
    private Double tax;
}
