package com.buralek.kafkasample.databaseservice.service.kafka.consumer;

import com.buralek.kafkasample.databaseservice.service.kafka.dto.WageKafkaDto;

public interface WageKafkaConsumer {
    void consume(WageKafkaDto dto);
}
