package com.buralek.kafkasample.databaseservice.service.kafka.dto;

import lombok.Data;

import java.util.Date;

@Data
public class WageKafkaDto {
    private String name;
    private String surname;
    private Double wage;
    private Date eventTime;
}
