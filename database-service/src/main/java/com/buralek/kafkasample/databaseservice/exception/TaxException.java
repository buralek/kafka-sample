package com.buralek.kafkasample.databaseservice.exception;

public class TaxException extends RuntimeException {
    public TaxException(String message) {
        super(message);
    }
}
