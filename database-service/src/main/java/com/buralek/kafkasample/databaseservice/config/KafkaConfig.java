package com.buralek.kafkasample.databaseservice.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class KafkaConfig {
    @Value("${kafka.server}")
    private String server;

    @Value("${kafka.consumer.group.id}")
    private String consumerGroupId;

    @Value("${kafka.consumer.topic}")
    private String consumerTopic;
}
