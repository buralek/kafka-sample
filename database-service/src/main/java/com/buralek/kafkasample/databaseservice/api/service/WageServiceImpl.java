package com.buralek.kafkasample.databaseservice.api.service;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;
import com.buralek.kafkasample.databaseservice.mapper.WageDtoMapper;
import com.buralek.kafkasample.databaseservice.service.database.repository.WageRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class WageServiceImpl implements WageService {
    private final WageDtoMapper wageDtoMapper;
    private final WageRepository wageRepository;

    @Override
    public List<WageDto> getAllWages() {
        return wageRepository.findAll().stream()
                .map(wageDtoMapper::map)
                .collect(Collectors.toList());
    }
}
