package com.buralek.kafkasample.databaseservice.api.controller;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;
import com.buralek.kafkasample.databaseservice.api.service.WageService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("wage-database")
@RequiredArgsConstructor
public class WageController {
    private final WageService wageService;

    @GetMapping("all")
    @Operation(summary = "Send all wages from database")
    public ResponseEntity<List<WageDto>> getAllWages() {
        log.info("Started to get all wages from database");
        List<WageDto> wages = wageService.getAllWages();
        log.info("All wages from database are '{}'", wages);
        return new ResponseEntity<>(wages, HttpStatus.OK);
    }
}
