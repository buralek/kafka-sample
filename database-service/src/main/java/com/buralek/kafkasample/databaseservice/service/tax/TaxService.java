package com.buralek.kafkasample.databaseservice.service.tax;

public interface TaxService {
    Double getWageWithTax(final Double wage);
}
