package com.buralek.kafkasample.databaseservice.service.database.repository;

import com.buralek.kafkasample.databaseservice.service.database.model.WageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WageRepository extends JpaRepository<WageEntity, Long> {
}
