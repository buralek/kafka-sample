package com.buralek.kafkasample.databaseservice.service.tax;

import com.buralek.kafkasample.databaseservice.exception.TaxException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
@Slf4j
public class TaxServiceImpl implements TaxService {
    private final Double tax;

    public TaxServiceImpl(Environment env) {
        final String taxPath = env.getProperty("service-settings.tax-path");
        log.info("Tax Path is {}", taxPath);
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final Tax taxObject = objectMapper.readValue(new File(taxPath), Tax.class);
            if (taxObject.getTax() == null) {
                throw new RuntimeException("Tax is null");
            } else if (taxObject.getTax() < 0) {
                throw new RuntimeException("Tax can't be less 0");
            }
            this.tax = taxObject.getTax();
            log.info("Tax is {}", tax);
        } catch (IOException exception) {
            throw new TaxException("Can't parse file with taxes. Exception is " + exception);
        }
    }
    @Override
    public Double getWageWithTax(final Double wage) {
        Double wageWithTax = wage + (wage * (tax/100));
        log.info("Wage with tax is {}", wageWithTax);
        return wageWithTax;
    }
}
