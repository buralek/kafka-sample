package com.buralek.kafkasample.databaseservice.service.kafka.consumer;

import com.buralek.kafkasample.databaseservice.config.KafkaConfig;
import com.buralek.kafkasample.databaseservice.mapper.WageDtoMapper;
import com.buralek.kafkasample.databaseservice.service.database.model.WageEntity;
import com.buralek.kafkasample.databaseservice.service.database.repository.WageRepository;
import com.buralek.kafkasample.databaseservice.service.kafka.dto.WageKafkaDto;
import com.buralek.kafkasample.databaseservice.service.tax.TaxService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class WageKafkaConsumerImpl implements WageKafkaConsumer {
    private final WageRepository wageRepository;
    private final WageDtoMapper wageDtoMapper;
    private final KafkaConfig kafkaConfig;
    private final TaxService taxService;

    @Override
    @KafkaListener(id = "database", topics = "#{kafkaConfig.getConsumerTopic()}", containerFactory = "singleFactory")
    public void consume(final WageKafkaDto wageKafkaDto) {
        log.info("Consumed {}", wageKafkaDto);
        Double wageWithTax = taxService.getWageWithTax(wageKafkaDto.getWage());
        wageKafkaDto.setWage(wageWithTax);
        WageEntity wageEntity = wageRepository.save(wageDtoMapper.map(wageKafkaDto));
        log.info("Saved {} into database", wageEntity);
    }
}
