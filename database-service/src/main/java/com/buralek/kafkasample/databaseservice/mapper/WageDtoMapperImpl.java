package com.buralek.kafkasample.databaseservice.mapper;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;
import com.buralek.kafkasample.databaseservice.service.database.model.WageEntity;
import com.buralek.kafkasample.databaseservice.service.kafka.dto.WageKafkaDto;
import org.springframework.stereotype.Service;

@Service
public class WageDtoMapperImpl implements WageDtoMapper {
    @Override
    public WageDto map(final WageEntity entity) {
        final WageDto wageDto = new WageDto();
        wageDto.setId(entity.getId());
        wageDto.setName(entity.getName());
        wageDto.setSurname(entity.getSurname());
        wageDto.setWage(entity.getWage());
        wageDto.setEventTime(entity.getEventTime());
        return wageDto;
    }

    @Override
    public WageEntity map(final WageKafkaDto wageKafkaDto) {
        final WageEntity wageEntity = new WageEntity();
        wageEntity.setName(wageKafkaDto.getName());
        wageEntity.setSurname(wageKafkaDto.getSurname());
        wageEntity.setWage(wageKafkaDto.getWage());
        wageEntity.setEventTime(wageKafkaDto.getEventTime());
        return wageEntity;
    }
}
