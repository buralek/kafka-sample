package com.buralek.kafkasample.databaseservice.mapper;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;
import com.buralek.kafkasample.databaseservice.service.database.model.WageEntity;
import com.buralek.kafkasample.databaseservice.service.kafka.dto.WageKafkaDto;

public interface WageDtoMapper {
    WageDto map(WageEntity entity);
    WageEntity map(WageKafkaDto wageKafkaDto);
}
