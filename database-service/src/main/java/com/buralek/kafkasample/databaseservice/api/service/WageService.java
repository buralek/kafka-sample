package com.buralek.kafkasample.databaseservice.api.service;

import com.buralek.kafkasample.databaseservice.api.dto.WageDto;

import java.util.List;

public interface WageService {
    List<WageDto> getAllWages();
}
