package com.buralek.kafkasample.rest.api.wage.controller;

import com.buralek.kafkasample.rest.api.wage.dto.WageDto;
import com.buralek.kafkasample.rest.api.wage.service.WageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WageController.class)
public class WageControllerTests {
    @MockBean
    private WageService wageService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void sendNewWageTest() throws Exception {
        final ObjectMapper objectMapper = new ObjectMapper();
        final String request = objectMapper.writeValueAsString(new WageDto());

        this.mockMvc.perform(post("/wage/send").contentType(MediaType.APPLICATION_JSON).content(request))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
