package com.buralek.kafkasample.rest.api.wage.service;

import com.buralek.kafkasample.rest.api.wage.dto.WageDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.Mockito.verify;

@SpringBootTest
public class WageServiceTests {
    private final static String KAFKA_WAGES_TOPIC = "KAFKA_WAGES_TOPIC";

    @Autowired
    private WageService wageService;

    @MockBean
    private KafkaTemplate<Long, WageDto> kafkaWageTemplate;

    @BeforeAll
    static void before() {
        System.setProperty("KAFKA_HOST", "kafka");
        System.setProperty("KAFKA_PORT", "9092");
        System.setProperty(KAFKA_WAGES_TOPIC, "wages");
    }

    @Test
    void sendNewWageTest() {
        WageDto wageDto = new WageDto();
        wageService.sendNewWage(wageDto);
        verify(kafkaWageTemplate).send(System.getProperty(KAFKA_WAGES_TOPIC), wageDto);
    }
}
