package com.buralek.kafkasample.rest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class KafkaSampleRestApplicationTests {

    @BeforeAll
    static void before() {
        System.setProperty("KAFKA_HOST", "kafka");
        System.setProperty("KAFKA_PORT", "9092");
        System.setProperty("KAFKA_WAGES_TOPIC", "wages");
    }

    @Test
    void contextLoads() {
    }

}
