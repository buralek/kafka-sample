package com.buralek.kafkasample.rest.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Producer REST service for Kafka sample project", version = "${service-settings.version}"))
public class OpenApiConfiguration {
}
