package com.buralek.kafkasample.rest.api.wage.controller;

import com.buralek.kafkasample.rest.api.wage.dto.WageDto;
import com.buralek.kafkasample.rest.api.wage.service.WageService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wage")
@Slf4j
@RequiredArgsConstructor
public class WageController {
    private final WageService wageService;

    @PostMapping("send")
    @Operation(summary = "Send new wage")
    public ResponseEntity sendNewWage(@RequestBody final WageDto request) {
        log.info("Started to send new wage with request '{}'", request);
        wageService.sendNewWage(request);
        log.info("Finished to send new wage with request '{}'", request);
        return ResponseEntity.ok().build();
    }
}
