package com.buralek.kafkasample.rest.api.wage.service;

import com.buralek.kafkasample.rest.api.wage.dto.WageDto;

public interface WageService {
    void sendNewWage(WageDto request);
}
