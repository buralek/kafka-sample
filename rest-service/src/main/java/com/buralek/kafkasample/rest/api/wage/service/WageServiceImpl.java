package com.buralek.kafkasample.rest.api.wage.service;

import com.buralek.kafkasample.rest.api.wage.dto.WageDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WageServiceImpl implements WageService {
    @Value("${kafka.producer.topic}")
    private String producerTopic;

    private final KafkaTemplate<Long, WageDto> kafkaWageTemplate;

    public WageServiceImpl(KafkaTemplate<Long, WageDto> kafkaWageTemplate) {
        this.kafkaWageTemplate = kafkaWageTemplate;
    }

    @Override
    public void sendNewWage(final WageDto request) {
        kafkaWageTemplate.send(producerTopic, request);
    }
}
