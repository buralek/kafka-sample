package com.buralek.kafkasample.rest.api.wage.dto;

import lombok.Data;

import java.util.Date;

@Data
public class WageDto {
    private String name;
    private String surname;
    private Double wage;
    private Date eventTime;
}
